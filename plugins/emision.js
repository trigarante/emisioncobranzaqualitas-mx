import axios from 'axios'

const emisionService = {}

emisionService.emitir = function(peticion) {
  return axios({
    method: 'post',
    headers:{'Content-Type': 'application/json'},
    url: process.env.urlWSAutos + '/emitir',
    data:  peticion
  })
    .then(res => res.data)
    .catch(err =>
      console.error('Ups... ' + aseguradora + ' no pudo emitir :( ' + err)
    )
}
export default emisionService
