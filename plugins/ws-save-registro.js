import axios from 'axios'

const emisionService = {}

emisionService.saveRegistro = function(peticion) {
  return axios({
    method: 'post',
    headers:{'Content-Type': 'application/json'},
    url: process.env.urlDB + '/saveRegistro',
    data:  peticion
  })
    .then(res => res.data)
    .catch(err =>
      console.error('Ups... no pudo guardar el registro :( ' + err)
    )
}
export default emisionService
