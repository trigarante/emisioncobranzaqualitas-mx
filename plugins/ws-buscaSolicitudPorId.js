import axios from 'axios'

const wsFindSolicitud = {}

wsFindSolicitud.getSolicitudById = function(idSolicitud) {
  return axios({
    method: 'get',
    url: process.env.urlDB + '/findSolicitudById?idSolicitud=' + idSolicitud
    //data: JSON.parse(idSolicitud)
  })
    .then(res => res.data)
    .catch(err =>
      console.error('Ups... Error al buscar la solicitud... :( ' + err)
    )
}
export default wsFindSolicitud
